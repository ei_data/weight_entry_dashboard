rm(list = ls())
library(dplyr)
library(readODS)
require("RPostgreSQL")
source("establish_connection.r")
centres <- dbGetQuery(con, "SELECT * from centre")
centres <- centres %>% arrange(name)
centre_select <- "sample_centre"
username_select <- "sample.user@delhivery.com"
admin_username <- "pne_admin"
wt_min_gm <- 100
wt_max_gm <- 500000
length_min_cm <- 1
breadth_min_cm <- 1
height_min_cm <- 1
length_max_cm <- 1000
breadth_max_cm <- 1000
height_max_cm <- 1000
limits <- "wt_min_gm <- 100
wt_max_gm <- 500000
length_min_cm <- 1
breadth_min_cm <- 1
height_min_cm <- 1
length_max_cm <- 1000
breadth_max_cm <- 1000
height_max_cm <- 1000"

download_date_select <- as.character(Sys.Date())
upload_file <- read.csv("sample_upload.csv",stringsAsFactors = F) %>% cbind(upload_time= Sys.time(),user_name = username_select,centre_name = centre_select,download_status = FALSE)
upload_file <- upload_file[wt_records_names]
# write_status <- dbWriteTable(con, "wt_records", value = upload_file, append = TRUE, row.names = FALSE)
duplicate_delete_query <- "DELETE FROM wt_records a USING ( SELECT max(upload_time) as upload_time, wbn,centre_name  FROM wt_records   GROUP BY wbn,centre_name HAVING COUNT(*) > 1) b WHERE a.wbn = b.wbn and a.centre_name = b.centre_name  AND a.upload_time <> b.upload_time"
dbSendQuery(con,duplicate_delete_query)
shiny::runApp(port = 9090)

