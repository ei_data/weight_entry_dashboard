library(shinydashboard)
library(shiny)
rm(ui)
rm(server)
ui <- dashboardPage(
  dashboardHeader(title = "Weight Entry"),
  dashboardSidebar(disable = T,sidebarMenu(
    #menuItem("Start Time", tabName = "starts", icon = icon("th")),
    #menuItem("Travel Time", tabName = "travels", icon = icon("th")),
    
  )),
  dashboardBody(
    # Boxes need to be put in a row (or column)
    tabsetPanel(
      # First tab content
      tabPanel(title = "Center Weight Data Upload",
               selectInput(
                 inputId = "weight_centre",label = "Centre",choices= unique(centres[,"name"])
                 ,selected = centres[1,"name"]
               ),textInput(inputId = "user_name", label = "User Name", value = ""),textOutput("valid_user_warning"),
               conditionalPanel(condition = "input.user_name != ''" ,
                                helpText(limits),
                                fluidRow(
                                  splitLayout(cellWidths = c("33%","33%","34%"), 
                                              cellArgs = list(style = "padding: 6px"),
                                              fileInput('file1', 'Choose CSV File',
                                                        accept=c('text/csv', 
                                                                 'text/comma-separated-values,text/plain', 
                                                                 '.csv','.ods')),radioButtons("filetype", "File type:",
                                                                                              c("csv" = "csv",
                                                                                                "ods" = "ods")),
                                              downloadButton('downloadSample', 'Download Sample Format')
                                  )),
                                # rejectRecords
                                actionButton(inputId = "start_submit",label = "Submit"),
                                h3(textOutput("upload_status")),
                                
                                # conditionalPanel(condition = "output.rejectRecordsRows",h2(downloadLink('rejectRecords', 'Download Rejected Rows'))),
                                conditionalPanel(condition = "output.rejectRecordsRows",h2(downloadLink('rejectRecords', 'Download Rejected Rows'))),
                                checkboxInput(inputId= "downloaded_or_not", label = "Include Data uploaded in previous sessions ?", value = FALSE),
                                
                                downloadButton('downloadData', 'Download to check data uploaded by you')
               )        
      ),
      tabPanel(title = "Admin Data Download",
               textInput(inputId = "admin_user",label = "Enter User ID",value= ""),
               textOutput("download_status"),
               
               conditionalPanel(condition = "input.admin_user == 'pne_admin'" ,
                                checkboxInput(inputId= "admin_downloaded_or_not", label = "Include Previously Downloaded Data ? \n Else, only Fresh Shipments will be downloaded.", value = FALSE),
                                textOutput("date_select_warning"),
                                
                                conditionalPanel(condition = "input.admin_downloaded_or_not == true" ,dateInput("download_date", "Start Date (only for historic data):", value = Sys.Date()),
                                                 dateInput("download_date_end", "End Date:", value = Sys.Date())),
                                
                                downloadButton('admin_downloadData', 'Download Consolidated Uploaded Weight Data')
               )
      )      
    )
  )
)

server <- function(input, output,session){
  rv <- reactiveValues()
  rv$upload_status_rv <- "Please Click Submit"
  rv$download_status_rv <- "You are not Authorized to Download this data. Please enter Admin Username"
  rv$current_output <- df_postgres
  rv$date_select_warning_rv <- ""
  rv$valid_user_warning_rv <- ""
  rv$reject_records_rv <- reject_records
  output$valid_user_warning <- renderText({
    if(input$user_name != "") rv$valid_user_warning_rv <- "Please upload file" else rv$valid_user_warning_rv <-  "Please enter a valid username"
    rv$valid_user_warning_rv
    
  })
  output$date_select_warning <- renderText({
    if(input$admin_downloaded_or_not) rv$date_select_warning_rv <- "Mandatory to select date for historic data" else rv$date_select_warning_rv <- ""
    rv$date_select_warning_rv
    
  })
  output$upload_status <- renderText({
    rv$upload_status_rv
  }) 
  output$download_status <- renderText({
    # rv$download_status_rv
    if(input$admin_user== admin_username) rv$download_status_rv <- "Please click on Download" 
    rv$download_status_rv
  }) 
  output$admin_downloadData <- downloadHandler(
    filename = function() { paste("weight_data","_",Sys.time(), ".",input$filetype, sep='') },
    content = function(file) {
      print(input$admin_user)
      if(input$admin_user== admin_username){
        download_date_select <- as.character(input$download_date)
        download_date_end_select <- as.character(input$download_date_end)
        if(input$admin_downloaded_or_not){
          q1 <- paste0("SELECT * from wt_records where upload_time::timestamp::date between  date'",download_date_select,"' and date'",download_date_end_select,"' order by upload_time")
          df_postgres <- dbGetQuery(con, q1)
          print(q1)
          dbSendQuery(con,"update wt_records set download_status = TRUE")
        }  else{
          q2 <- "SELECT * from wt_records where download_status = FALSE order by upload_time"
          df_postgres <- dbGetQuery(con, q2 )
          print(q2)
          dbSendQuery(con,paste0("update wt_records set download_status = TRUE where upload_time::timestamp::date between  date'",download_date_select,"' and date'",download_date_end_select))
        } 
        # dbSendQuery(con,"update wt_records set download_status = TRUE")
        # co <- rv$current_output
        rv$current_output <- df_postgres
        numrows_download <- nrow(rv$current_output)
        # if(input$admin_user== admin_username) rv$download_status_rv <- paste(numrows_download,"rows successfully downloaded" ) else "You are not Authorized to Download this data"
        if(input$filetype == "csv")
          write.csv(rv$current_output, file,row.names = F)
        if(input$filetype == "ods")
          write_ods(rv$current_output, file)
        
        
      } else{
        
        if(input$filetype == "csv")
          write.csv(data.frame(), file,row.names = F)
        if(input$filetype == "ods")
          write_ods(data.frame(), file)
        
      }
      
    }
  )
  output$downloadData <- downloadHandler(
    filename = function() { paste("weight_data","_",Sys.time(), ".",input$filetype,sep='') },
    content = function(file) {
      username_select <- input$user_name
      centre_select <- input$weight_centre
      q1 = paste0("SELECT * from wt_records where centre_name = '",centre_select,"' and user_name = '",username_select,"'  order by upload_time")
      q2= paste0("SELECT * from wt_records where centre_name = '",centre_select,"' and user_name = '",username_select,"' and download_status = FALSE order by upload_time")
      if(input$downloaded_or_not) df_postgres <- dbGetQuery(con,q1 ) else df_postgres <- rv$current_output
      # dbSendQuery(con,"update wt_records set download_status = TRUE")
      # co <- rv$current_output
      # rv$current_output <- df_postgres
      if(input$filetype == "csv")
        write.csv(df_postgres, file,row.names = F)
      if(input$filetype == "ods")
        write_ods(df_postgres, file)
      
    }
  )
  output$downloadSample <- downloadHandler(
    filename = function() { paste("sample_weight_data","_",Sys.time(),".",input$filetype, sep='') },
    content = function(file) {
      if(input$filetype == "csv"){
        print("input$filetype csv")
        write.csv(sample_upload, file,row.names = F)
      }
      
      if(input$filetype == "ods"){
        print("input$filetype ods")
        write_ods(sample_upload, file)
      }
      
      
    }
  )
  output$rejectRecordsRows <- reactive({(nrow(rv$reject_records_rv))})
  # output$rejectRecordsRows <- reactive({1})
  
  #   output$nrows <- reactive({
  #     1# nrow(datasetInput())
  #   })
  outputOptions(output, "rejectRecordsRows", suspendWhenHidden = FALSE)  
  output$rejectRecords <- downloadHandler(
    filename = function() { paste("rejected_data","_",Sys.time(), ".",input$filetype, sep='') },
    content = function(file) {
      if(input$filetype == "csv")
        write.csv(rv$reject_records_rv, file,row.names = F)
      if(input$filetype == "ods")
        write_ods(rv$reject_records_rv, file)
    }
  )
  observeEvent(input$start_submit, {
    inFile <- input$file1
    print(paste("input$filetype",input$filetype) )
    print(input$filetype == "csv")
    username_select <- input$user_name
    centre_select <- input$weight_centre
    if (is.null(inFile))
      return(NULL)
    
    
    print("inFile")
    print(inFile)
    print("inFile$datapath")
    print(input$file1$name)
    if(!grepl(input$filetype,input$file1$name)){
      rv$upload_status_rv <- "Uploaded file is not in the required format"
      return(NULL)
    }
    if(input$filetype == "csv")
      upload_file <-    read.csv(input$file1$datapath,stringsAsFactors = F) 
    if(input$filetype == "ods")
      upload_file <-    read_ods(input$file1$datapath) 
    
    if(!identical(wt_upload_names,names(upload_file))){
      # print("names of upload_file")
      # print(names(upload_file))
      # print("wt_upload_names")
      # print(wt_upload_names)
      print("upload_file")
      # print(upload_file)
      rv$upload_status_rv <- "Uploaded file is not in the required format"
      return(NULL)
    }
    
    upload_status_message_trycatch = tryCatch({
      # print("col names match : ")
      # print(identical(wt_upload_names,names(upload_file)))
      # print("upload_file")
      # print(upload_file)
      
      upload_file <- upload_file[wt_upload_names] %>% cbind(upload_time= Sys.time(),user_name = username_select,centre_name = centre_select,download_status = FALSE)
      print(0)
      upload_file <- upload_file[wt_records_names]
      print(head(upload_file))
      # print(1)
      upload_file_freeze <- upload_file
      # upload_file <- suppressWarnings(upload_file %>% mutate(length= as.numeric(length),breadth = as.numeric(breadth),height = as.numeric(height),wt = as.numeric(wt)))
      reject_records <- upload_file[0,]
      reject_records <- suppressWarnings(upload_file %>% filter(is.na(as.numeric(wt))) %>% mutate(reject_reason = "wt column not in required format or empty") )
      reject_records <- suppressWarnings(upload_file %>% filter(is.na(as.numeric(length))) %>% mutate(reject_reason = "length column not in required format or empty") %>% rbind(reject_records))
      print(1.1)
      print(head(reject_records))
      print(1.11)
      print(suppressWarnings(upload_file %>% filter(is.na(as.numeric(breadth))) %>% mutate(reject_reason = "breadth column not in required format or empty")))
      print(1.12)
      reject_records <- suppressWarnings(upload_file %>% filter(is.na(as.numeric(breadth))) %>% mutate(reject_reason = "breadth column not in required format or empty") %>% rbind(reject_records))
      print(1.2)
      reject_records <- suppressWarnings(upload_file %>% filter(is.na(as.numeric(height))) %>% mutate(reject_reason = "height column not in required format or empty") %>% rbind(reject_records))
      print(1.3)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(wt) < wt_min_gm) %>% mutate(reject_reason = paste("wt cannot be lesser than",wt_min_gm)) %>% rbind(reject_records))
      print(1.4)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(wt) > wt_max_gm) %>% mutate(reject_reason = paste("wt cannot be greater than",wt_max_gm)) %>% rbind(reject_records))
      print(1.5)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(length) < length_min_cm) %>% mutate(reject_reason = paste("length cannot be lesser than",length_min_cm)) %>% rbind(reject_records))
      print(1.6)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(length) > length_max_cm) %>% mutate(reject_reason = paste("length cannot be greater than",length_max_cm)) %>% rbind(reject_records))
      print(1.7)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(breadth) < breadth_min_cm) %>% mutate(reject_reason = paste("breadth cannot be lesser than",breadth_min_cm)) %>% rbind(reject_records))
      print(1.3)
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(breadth) > breadth_max_cm) %>% mutate(reject_reason = paste("breadth cannot be greater than",breadth_max_cm)) %>% rbind(reject_records))
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(height) < height_min_cm) %>% mutate(reject_reason = paste("height cannot be lesser than",height_min_cm)) %>% rbind(reject_records))
      # reject_records <- suppressWarnings(upload_file %>% filter(grepl("\\+",toupper(wbn))) %>% mutate(reject_reason = paste("wbn name cannot have "+"",height_max_cm)) %>% rbind(reject_records))
      reject_records <- suppressWarnings(upload_file %>% filter(as.numeric(height) > height_max_cm) %>% mutate(reject_reason = paste("height cannot be greater than",height_max_cm)) %>% rbind(reject_records))
      
      reject_records <- suppressWarnings(upload_file %>% filter(grepl("\\+",wbn)) %>% mutate(reject_reason = paste("wbn cannot contain '+'")) %>% rbind(reject_records))
      
      
      reject_records <- suppressWarnings(reject_records %>% group_by(user_name,centre_name,wbn,wt,cwt,length,breadth,height,upload_time,download_status)%>% summarise(reject_reason= paste(reject_reason,collapse= "; "))%>% as.data.frame())
      reject_records <- reject_records[c(wt_upload_names,"reject_reason")]
      upload_file <- suppressWarnings(upload_file %>% filter(!is.na(as.numeric(wt))) %>% filter(!is.na(as.numeric(length))) %>% filter(!is.na(as.numeric(breadth))) %>% filter(!is.na(as.numeric(height))) %>%
                                        filter(!as.numeric(wt) < wt_min_gm) %>% filter(!as.numeric(wt) > wt_max_gm) %>% 
                                        filter(!as.numeric(length) < length_min_cm) %>% filter(!as.numeric(length) > length_max_cm) %>% 
                                        filter(!as.numeric(breadth) < breadth_min_cm) %>% filter(!as.numeric(breadth) > breadth_max_cm) %>%
                                        filter(!as.numeric(height) < height_min_cm) %>% filter(!as.numeric(height) > height_max_cm) %>% filter(!grepl("\\+",wbn))) %>% 
        
        print(1.5)
      print(head(upload_file))
      write_status <- dbWriteTable(con, "wt_records", value = upload_file, append = TRUE, row.names = FALSE)
      print(2)
      print(write_status)
      dbSendQuery(con,duplicate_delete_query)
      print(3)
      numrows <- nrow(upload_file)
      print(4)
      if(write_status)  upload_status_message <- paste(numrows,"rows successfully uploaded by",username_select,"from",centre_select) else upload_status_message <- "Failure"
      if(write_status & nrow(reject_records) > 0) upload_status_message <- paste0(upload_status_message,"\n",nrow(reject_records)," rows rejected. Please check and upload these rows again")
      print(5)
      df_postgres <- dbGetQuery(con, "SELECT * from wt_records order by upload_time")
      print(6)
      # rv$current_output <- df_postgres
      rv$current_output <- upload_file
      rv$reject_records_rv <- reject_records
      upload_status_message
    }, warning = function(w) {
      w
    }, error = function(e) {
      e
    }, finally = {
      print("cleanup-code")
    })
    rv$upload_status_rv <- upload_status_message_trycatch
    
    
    
    
  })
  #   observeEvent(input$downloaded_or_not,{
  #     df_postgres <- dbGetQuery(con, "SELECT * from wt_records where download_status = FALSE")
  #     rv$current_output <- df_postgres
  #     
  #   })
  
}
shinyApp(ui, server)
