import psycopg2
#note that we have to import the Psycopg2 extras library!
import psycopg2.extras
import sys
import urllib2
import json
 
def main():
    conn_string = "host='localhost' dbname='weight_entry' user='postgres' password='delhivery'"
    # print the connection string we will use to connect
    print "Connecting to database\n ->%s" % (conn_string)
 
    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(conn_string)
 
    # conn.cursor will return a cursor object, you can use this query to perform queries
    # note that in this example we pass a cursor_factory argument that will
    # dictionary cursor so COLUMNS will be returned as a dictionary so we
    # can access columns by their name instead of index.
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
 
    # tell postgres to use more work memory
    work_mem = 2048
 
    # by passing a tuple as the 2nd argument to the execution function our
    # %s string variable will get replaced with the order of variables in
    # the list. In this case there is only 1 variable.
    # Note that in python you specify a tuple with one item in it by placing
    # a comma after the first variable and surrounding it in parentheses.
    # Then we get the work memory we just set -> we know we only want the
    # first ROW so we call fetchone.
    # then we use bracket access to get the FIRST value.
    # Note that even though we've returned the columns by name we can still
    # access columns by numeric index as well - which is really nice.
    url="https://api.delhivery.com/api/dc/fetch/dc/list.json"
    request=urllib2.Request(url,None,{'Content-Type':'application/json','Accept':'application/json','Authorization':'token 07e3eb7e407e2a9c98c51be1a01c6dec77418137'})
    resp=urllib2.urlopen(request)
    j=json.loads(resp.read())
    for item in j:
        name=item['name']
        hq_name=item['unicode_name']
        code=item['code']
        city=item['city']
        active=item['active']
        latitude=item['latitude'] or 0.0
        longitude=item['longitude']  or 0.0
        if item['facility_type']['HUB']:
            itype="Hub"
        elif item['facility_type']['IPC']:
            itype='IPC'
        elif item['facility_type']['DC']:
            itype='DC'
        elif item['facility_type']['DPC']:
            itype='DPC'
        elif item['facility_type']['RPC']:
            itype='RPC'
        else:
            itype=''
        cursor.execute("SELECT * FROM CENTRE WHERE HQ_CODE LIKE '%s'"%code)
        row=cursor.fetchone() 
        if row:
             cursor.execute("UPDATE CENTRE SET name='%s',city='%s',type='%s' ,last_update=current_timestamp,hq_name='%s' WHERE HQ_CODE LIKE '%s' " %(name,city,itype,code,hq_name))
        else:
             cursor.execute( "INSERT INTO CENTRE (name,hq_code,latitude,longitude,created_on,last_update,city,type,hq_name) VALUES ('%s','%s',%s,%s,current_timestamp,current_timestamp,'%s','%s','%s')" %(name,code,latitude,longitude,city,itype,hq_name))
    # Call fetchone - which will fetch the first row returned from the
    cursor.execute("update centre set city = 'Delhi' where city = 'Gurgaon'")
    cursor.execute("update centre set search_index=hq_code||'_'||name||'_'||city||'_'||type")
    conn.commit()
    # database.
    # access the column by numeric index:
    # even though we enabled columns by name I'm showing you this to
    # show that you can still access columns by index and iterate over them.
 
    # print the entire row 
 
if __name__ == "__main__":
    main()