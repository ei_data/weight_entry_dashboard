create table wt_records (
user_name text ,
centre_name text,
wbn text,
wt numeric,
cwt numeric,
length numeric,
breadth numeric,
height numeric,
upload_time timestamp without time zone,
download_status boolean);

CREATE TABLE centre (
    name character varying(255),
    abbr character varying(255),
    hq_code character varying(255),
    latitude double precision,
    longitude double precision,
    address_id bigint,
    created_on timestamp without time zone NOT NULL,
    last_update timestamp without time zone NOT NULL,
    city character varying(255),
    type character varying(255),
    parent_centre_id bigint,
    search_index text,
    hq_name character varying(255),
    ist_create boolean DEFAULT false,
    ist_receive boolean DEFAULT false,
    bag_receive boolean DEFAULT false
);


ALTER TABLE centre OWNER TO postgres;
