
upload_file <-    read.csv("sample_upload.csv",stringsAsFactors = F) 
if(!identical(wt_upload_names,names(upload_file))){
  rv$upload_status_rv <- "Uploaded file is not in the required format"
  return(NULL)
}
upload_status_message_trycatch = tryCatch({
  print("col names match : ")
  print(identical(wt_upload_names,names(upload_file)))
  upload_file <- upload_file[wt_upload_names] %>% cbind(upload_time= Sys.time(),user_name = username_select,centre_name = centre_select,download_status = FALSE)
  upload_file <- upload_file[wt_records_names]
  print(upload_file)
  print(1)
  reject_records <- upload_file[0,]
  reject_records <- upload_file %>% filter(is.na(as.numeric(wt))) %>% mutate(reject_reason = "wt column not in required format or empty") %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(is.na(as.numeric(length))) %>% mutate(reject_reason = "length column not in required format or empty") %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(is.na(as.numeric(breadth))) %>% mutate(reject_reason = "breadth column not in required format or empty") %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(is.na(as.numeric(height))) %>% mutate(reject_reason = "height column not in required format or empty") %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(wt) < wt_min_gm) %>% mutate(reject_reason = paste("wt cannot be lesser than",wt_min_gm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(wt) > wt_max_gm) %>% mutate(reject_reason = paste("wt cannot be greater than",wt_max_gm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(length) < length_min_cm) %>% mutate(reject_reason = paste("length cannot be lesser than",length_min_cm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(length) > length_max_cm) %>% mutate(reject_reason = paste("length cannot be greater than",length_max_cm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(breadth) < breadth_min_cm) %>% mutate(reject_reason = paste("breadth cannot be lesser than",breadth_min_cm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(breadth) > breadth_max_cm) %>% mutate(reject_reason = paste("breadth cannot be greater than",breadth_max_cm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(height) < height_min_cm) %>% mutate(reject_reason = paste("height cannot be lesser than",height_min_cm)) %>% rbind(reject_records)
  reject_records <- upload_file %>% filter(as.numeric(height) > height_max_cm) %>% mutate(reject_reason = paste("height cannot be greater than",height_max_cm)) %>% rbind(reject_records)
  reject_records <- reject_records %>% group_by(user_name,centre_name,wbn,wt,cwt,length,breadth,height,upload_time,download_status)%>% summarise(reject_reason= paste(reject_reason,collapse= "; "))%>% as.data.frame()
  upload_file <- upload_file %>% filter(!is.na(as.numeric(wt))) %>% filter(!is.na(as.numeric(length))) %>% filter(!is.na(as.numeric(breadth))) %>% filter(!is.na(as.numeric(height))) %>%
    filter(!as.numeric(wt) < wt_min_gm) %>% filter(!as.numeric(wt) > wt_max_gm) %>% 
    filter(!as.numeric(length) < length_min_cm) %>% filter(!as.numeric(length) > length_max_cm) %>% 
    filter(!as.numeric(breadth) < breadth_min_cm) %>% filter(!as.numeric(breadth) > breadth_max_cm) %>%
    filter(!as.numeric(height) < height_min_cm) %>% filter(!as.numeric(height) > height_max_cm)
  write_status <- dbWriteTable(con, "wt_records", value = upload_file, append = TRUE, row.names = FALSE)
  
  print(2)
  print(write_status)
  dbSendQuery(con,duplicate_delete_query)
  print(3)
  numrows <- nrow(upload_file)
  print(4)
  if(write_status)  upload_status_message <- paste(numrows,"rows successfully uploaded by",username_select,"from",centre_select) else upload_status_message <- "Failure"
  print(5)
  df_postgres <- dbGetQuery(con, "SELECT * from wt_records order by upload_time")
  print(6)
  # rv$current_output <- df_postgres
  upload_status_message
}, error = function(e) {
  e
}, finally = {
  print("cleanup-code")
})
# rv$upload_status_rv <- upload_status_message_trycatch


reject_records[wt_upload_names]
