library(dplyr)
# install.packages("RPostgreSQL")
require("RPostgreSQL")
pp <- getwd()
pw <- {
  "delhivery"
}
system(paste0("export PGPASSWORD=delhivery\n","psql -U postgres -h localhost -d weight_entry -p 5432 -w  -a -f ",pp,"/wt_ddl.sql"))
system(paste0("python ",pp,"/centre.py"))
# create a connection
# save the password that we can "hide" it as best as we can by collapsing it

# loads the PostgreSQL driver
drv <- dbDriver("PostgreSQL")
# creates a connection to the postgres database
# note that "con" will be used later in each connection to the database
con <- dbConnect(drv, dbname = "weight_entry",
                 host = "localhost", port = 5432,
                 user = "postgres", password = pw)
rm(pw) # removes the password

# check for the cartable
dbExistsTable(con, "wt_records")
# TRUE
sample_upload <- read.csv("sample_upload.csv",stringsAsFactors = F)
df <- read.csv("sample_entry.csv",stringsAsFactors = F) %>% cbind(upload_time = Sys.time())
# writes df to the PostgreSQL database "postgres", table "cartable" 
dbWriteTable(con, "wt_records", 
             value = df, append = TRUE, row.names = FALSE)

# query the data from postgreSQL 
df_postgres <- dbGetQuery(con, "SELECT * from wt_records")
wt_records_names <- names(df_postgres)
# compares the two data.frames
identical(df, df_postgres)
# TRUE
