pp <- getwd()
pw <- {
  "delhivery"
}
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname = "weight_entry",
                 host = "localhost", port = 5432,
                 user = "postgres", password = pw)
rm(pw) # removes the password

# check for the cartable
dbExistsTable(con, "wt_records")
# TRUE
df_postgres <- dbGetQuery(con, "SELECT * from wt_records")
wt_records_names <- names(df_postgres)

sample_upload <- read.csv("sample_upload.csv",stringsAsFactors = F)
wt_upload_names <- names(sample_upload)
reject_records <- sample_upload[0,] %>% mutate(reject_reason = "")
